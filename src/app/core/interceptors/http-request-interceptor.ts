import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpResponse
  } from '@angular/common/http';
  import { Router } from '@angular/router';
  import { Observable, throwError } from 'rxjs';
  import { catchError, tap } from 'rxjs/operators';

  import { AuthState } from '@store/auth.state';
  import { Store } from '@ngxs/store';
  import { environment } from '../../../environments/environment';


  @Injectable()

  export class HttpRequestInterceptor implements HttpInterceptor {
    constructor(private router: Router, private store: Store) {}
  
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const token = this.store.selectSnapshot(AuthState.token);
  
      if (token) {
        req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
      }

      if (!req.headers.has('Content-Type')) {
        if (req.url == `${environment.server}/posts` || `${environment.server}/users/edit`) {
          delete req['Content-Type'];
        } else {
          req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }
      }
  
      req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
      return next.handle(req).pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // this.hideLoader();
          }
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
              /*
                Insert Alert here
            */
            if (err.status === 401) {
            /*
                Insert Alert here
            */
            console.log('errror');
            }
            if (err.status === 500 && req.method === 'GET') {
              this.router.navigate(['error']);
            }
            if (!window.navigator.onLine) {
              this.router.navigate(['no-internet-connection']);
            }
            if (err.status !== 401) {
              /*
                Insert Alert here
            */
            console.log('error');
            }
            return throwError(err);
          }
        })
      );
    }
  }