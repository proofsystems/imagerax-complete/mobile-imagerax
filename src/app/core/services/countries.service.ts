import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Country } from '../../models/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  url = `${environment.server}`;

  constructor(
    private http:HttpClient
  ) { }

  getAll() {
    return this.http.get<Country[]>(`${this.url}/countries/all`);
  }


}
