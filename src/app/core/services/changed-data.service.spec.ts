import { TestBed } from '@angular/core/testing';

import { ChangedDataService } from './changed-data.service';

describe('ChangedDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChangedDataService = TestBed.get(ChangedDataService);
    expect(service).toBeTruthy();
  });
});
