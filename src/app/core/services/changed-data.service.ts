import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {
  pubickeySiriusid = '';
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  /**
   * For TEST_NET
   */
  publicKeydApp = '10F4337C7A862A247435167AEF4BD37BF5FAE8BCB247A8B3B3B1A616F9224708';
  privateKeydApp = 'FC4979544C6D504AE8181FB0CD23C68FB345FB3FDC3DC232D419AAF2D3581248';
  addressdApp: Address;
  apiNode = '';
  ws = 'wss://demo-sc-api-1.ssi.xpxsirius.io';
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
  updateWebsocket() {
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() {
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.PRIVATE_TEST);
  }
}
