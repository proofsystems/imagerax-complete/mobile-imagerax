import { TestBed } from '@angular/core/testing';

import { ProximaxService } from './proximax.service';

describe('ProximaxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProximaxService = TestBed.get(ProximaxService);
    expect(service).toBeTruthy();
  });
});
