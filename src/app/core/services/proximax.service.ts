import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { HttpClient } from '@angular/common/http';
import {
  Account,
  NetworkType,
  Address,
  AccountHttp,
  QueryParams,
  PublicAccount,
  MosaicHttp,
  MosaicService,
  TransferTransaction,
  Transaction,
  Password,
  SimpleWallet
} from 'tsjs-xpx-chain-sdk';

import { Store } from '@ngxs/store';
import { EditProfile } from '@store/auth.action';

@Injectable({
  providedIn: 'root'
})
export class ProximaxService {

  // node:any = environment.nodeUrl
  NETWORK_TYPE : NetworkType;

  address:any;
  publicKey:any;

  constructor(
      private http:HttpClient,
      private store: Store
  ) { 

    this.NETWORK_TYPE = NetworkType.TEST_NET

  }

  public generateNewAccount = () => {
    const newAccount = Account.generateNewAccount(this.NETWORK_TYPE);
    return newAccount;
  }

  public createWallet = (_wallet_name: any, _wallet_password: any) => {
    const password = new Password(_wallet_password);
    const wallet = SimpleWallet.create(_wallet_name, password, NetworkType.TEST_NET);
    const account = wallet.open(password);
    // store address and public_key
    return this.onEditProfile((wallet.address.pretty() ), account.publicKey, JSON.stringify(wallet));
  }

  onEditProfile(address,public_key,wallet){
    const formData = new FormData();
    formData.append('wallet_address', address);
    formData.append('wallet_public_key', public_key);
    formData.append('wallet', wallet);
    this.store.dispatch(new EditProfile(formData)).subscribe(() => {

    },
      err => {
          console.log(err)
      }
    )
}


  public linkWallet = (_wallet_privateKey: any, _wallet_name: any, _wallet_password: any) => {
    const password = new Password('password');
    const privateKey = _wallet_privateKey as string;
    const wallet = SimpleWallet.createFromPrivateKey(_wallet_name, password, privateKey, NetworkType.TEST_NET);
    const account = wallet.open(password);
    return this.onEditProfile((account.address.pretty()), account.publicKey, JSON.stringify(wallet));
  }


  getAccountInfo(_addr) {
    if (_addr == null) {
      // console.log('---------I have NO ACCOUNT');
      return true;
    }
    else {
      // console.log('---------I have ACCOUNT, ');
      return false;
    }
  }

}
