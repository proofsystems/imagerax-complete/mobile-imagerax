import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable,of } from 'rxjs';
import { Auth } from '@models/auth.model';
import { Store, Select } from '@ngxs/store';
import { AuthState } from '@store/auth.state';
import { User } from '@models/user.model';
import { BoardStateModel,Board } from '@models/board.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = `${environment.server}`


  constructor(
    private http:HttpClient
  ) { }

  getUserOwnProfile(){
      return this.http.get(`${this.url}/auth/me`);
  }

  getUserBooardOwned(){
      return this.http.get<Board[]>(`${this.url}/boards/owned`);
  }

  getUserBoardById(id:number){
      return this.http.get(`${this.url}/boards/${id}`)
  }

  viewUserProfile(id:number){
      return this.http.get(`${this.url}/users/${id}`);
  }

  followUser(id:number){
      return this.http.get(`${this.url}/followers/follow/${id}`)
  }

  getFollowers(id:number){
      return this.http.get<User[]>(`${this.url}/users/${id}/followers`);
  }

  getFollowing(id:number){
    return this.http.get<User[]>(`${this.url}/users/${id}/following`);
  }

  onFollowUser(id:number){
      return this.http.post(`${this.url}/followers/follow/${id}`,JSON.stringify(id))
  }

  updateProfile(payload:FormData){
      return this.http.post(`${this.url}/users/edit`,payload)
  }









}
