import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Notification } from '@models/notification.model';





@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  url = `${environment.server}`;


  constructor(
    private http:HttpClient
  ) { }

  getRecent10(){
      return this.http.get<Notification[]>(`${this.url}/users/notifications`);
  }

  getAllNotications(){
      return this.http.get<Notification[]>(`${this.url}/users/notifications`);
  }

}
