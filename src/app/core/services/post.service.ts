import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Post } from '../../models/post.model';
import { Comment } from '@models/comment.model';
import { throttleTime } from 'rxjs/operators';
import { Board } from '@models/board.model';
import {  Feeds } from '@models/feeds.model';
 
@Injectable({
  providedIn: 'root'
})
export class PostService {

  url = `${environment.server}`;

  constructor(
    private http:HttpClient
  ) { }

  getAll() {
    // return this.http.get<Post[]>(`${this.url}/posts/all`);
     return this.http.get<Post[]>(`${this.url}/posts/category-feeds`);
  }

  private getPostUrl(id: number) {
    return `${this.url}/posts/${id}`;
  }

  getPostById(id:number){
      return this.http.get(this.getPostUrl(id));
  }

  commentPost(payload:Comment){
      return this.http.post(`${this.url}/comments`,payload);
  }

  pinPost(payload:Board){
      return this.http.post(`${this.url}/posts/pin`,payload)
  }

  createBoard(payload:Board){
      return this.http.post(`${this.url}/boards`,payload)
  }

  createPost(payload:FormData){
      return this.http.post(`${this.url}/posts`,payload);
  }

  likePost(payload:Post){
      return this.http.get(`${this.url}/posts/like/${payload}`);
  }

  deletePost(payload:Post){ 
      return this.http.delete(`${this.url}/posts/${payload}`);
  }

  getOwnPost(){
      return this.http.get(`${this.url}/posts/own`);
  }

  getFeeds(){
    //   return this.http.get(`${this.url}/posts/feeds`);
    return this.http.get<Feeds[]>(`${this.url}/posts/feeds`)
  }

  getPostPerPage(key:string,pageNumber:string){
    return this.http.get<Post[]>(`${this.url}/posts/?key=${key}&page=${pageNumber}`);

  }

  getAllTrends(){
      return this.http.get<Post[]>(`${this.url}/posts/trends`)
  }

  getAllTrendsToday(data:string){
      return this.http.get<Post[]>(`${this.url}/posts/trends/${data}`)
  }

  getAllFavorites(){
      return this.http.get<Post[]>(`${this.url}/posts/favorites`)
  }

  getAllFavoritesToday(data:string){
      return this.http.get<Post[]>(`${this.url}/posts/favorites/${data}`)
  }

  getPostImageById(id:number){
      return this.http.get<Post[]>(`${this.url}/posts/${id}`)
  }

  getAllBoard(){
      return this.http.get<Board[]>(`${this.url}/boards/all`)
  }



  

}
