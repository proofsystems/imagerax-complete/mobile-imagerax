import { Injectable, ɵbypassSanitizationTrustResourceUrl } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable,of } from 'rxjs';
import { Auth } from '@models/auth.model';
import { Store, Select } from '@ngxs/store';
import { AuthState } from '@store/auth.state';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = `${environment.server}`

  constructor(
    private http:HttpClient,
    private store: Store
  ) { }

    login(payload:Auth){
        return this.http.post(`${this.url}/auth/login`,payload)
    }


    register(payload:Auth){
      return this.http.post(`${this.url}/auth/register`,payload)
    }

    editProfile(payload:FormData){
      return this.http.post(`${this.url}/users/edit`,payload)
    }

    isLoggedIn():boolean {
        const token = this.store.selectSnapshot(AuthState.token)
        if(!token) {
            return false;
        }
        return true;
    }

    getUserId():boolean {
       const userId = this.store.selectSnapshot(AuthState.getUserId)
       if (!userId) {
            return false;
       } 
       return true;
    }

    userId(){
      const userId = this.store.selectSnapshot(AuthState.getUserId)
      return userId;
    }

    getIProfileImage(){
      const image = this.store.selectSnapshot(AuthState.getProfile)
      return image;
    }

   


}
