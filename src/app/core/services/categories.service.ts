import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Categories } from '@models/categories.model';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url = `${environment.server}`;

  constructor(private http:HttpClient) {
   
   }

   getAllCategories() {
    return this.http.get<Categories[]>(`${this.url}/categories/all`);
  }

  selectCategories(payload) {
    return this.http.post(`${this.url}/user-categories`, payload);
  }

}
