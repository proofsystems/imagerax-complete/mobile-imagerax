import { LoadingController,ToastController,AlertController} from '@ionic/angular';
import { Injectable } from '@angular/core';
import { GetAllPost } from '@store/post/post.action';
import { GetUserOwnProfile } from '@store/auth.action';
import { Store } from '@ngxs/store';
import { GetAllBoards } from '@store/post/post.action';
  

@Injectable()

export class AlertProvider{
    
    isLoading = false;
    constructor(
        private load: LoadingController,
        private toast: ToastController,
        private alert: AlertController,
        private store:Store
        ) {

    }

    async registerToast() {
      const toast = await this.toast.create({
        message: 'Your account has been register.',
        duration: 1500,
        color:'success',
      });
      toast.present();
    }

    async sliderNotification() {
      const toast = await this.toast.create({
        message: 'Slide to Put Caption',
        duration: 900,
        color:'success',
      });
      toast.present();
    }

    async selectedCategory(){
      const load = await this.load.create({
        message: 'Selected a Category',
        duration: 1000
      });
      await load.present();
      this.store.dispatch(new GetAllPost());
    }

    async updateProfile(){
      const load = await this.load.create({
        message: 'Profile Sucessfully Updated',
        duration: 1000
      });
      await load.present();
      this.store.dispatch(new GetUserOwnProfile());
    }

    async alertController(data){
        const toast = await this.alert.create({
            header: 'Alert',
            subHeader: 'Subtitle',
            message: data,
            buttons: ['OK']
          });
          await toast.present();
    }

    async toastController(data){
        const toast = await this.toast.create({
            message: data,
            duration: 2000
          });
          toast.present();
    }

    async deletePost(data){
      const load = await this.load.create({
        message: 'Deleting Post',
        duration: 1000
      });
      await load.present();
      this.store.dispatch(new GetAllPost());

    }

    async loadingController(data){
        const load = await this.load.create({
            message: data,
            duration: 900
          });
          await load.present();
    }

    async feeds(){
      const load = await this.load.create({
          message: 'NO Available Feeds',
          duration: 1000
        });
        await load.present();
  }

    async boardCreated(data){
      const load = await this.load.create({
          message: data,
          duration: 1000
        });
        await load.present();
        this.store.dispatch(new GetAllBoards())
  }

  async postCreated(data){
    const load = await this.load.create({
      message: data,
      duration: 1000
    });
    await load.present();
    this.store.dispatch(new GetAllPost());
  }
    
    async loader(){
        const loading = await this.load.create({
            message: 'Gathering data... Please Wait'
        });
        await loading.present();
    }

    async login(){
      const loading = await this.load.create({
          message: 'Successfully Login',
          duration: 1000
      });
      await loading.present();
  }

  async closeModalFollower(){
      const loading = await this.load.create({
        message: 'Successfully Login',
        duration: 1000
    });
    await loading.present();
  }

  async loginError(){
    const loading = await this.load.create({
        message: 'Check Email and Password',
        duration: 1000
    });
    await loading.present();
}

    async loginLoader(){
        this.isLoading = true;
        return await this.load.create({
          duration: 1000,
          spinner: 'bubbles',
          cssClass:'my-loading-class',
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

    async loginErrorLoader(data){
        const loading = await this.load.create({
            message: data,
            duration:1000
        });
        await loading.present();
    }



    async signInErrorLoader(){
        const loading = await this.load.create({
            message: 'Wrong Credentials, Something Went Wrong',
            duration:1000
        });
        return await loading.present();
    }

    async forgotPassNotif(){
        const loading = await this.load.create({
            message: 'Email has been Sent',
            duration:1000
        });
        return await loading.present();
}

    async passError(data){
        const loading = await this.load.create({
            message: data,
            duration:2000
        });
        return await loading.present();
    }

    async proceedToProfile(){
        const loadingController = document.querySelector('ion-loading-controller');
        await loadingController.componentOnReady();
      
        const loadingElement = await loadingController.create({
          message: 'Please wait...',
          spinner: 'crescent',
          duration: 2000
        });
        return await loadingElement.present();
    }

    async noSearch(){
        const toast = await this.alert.create({
            header: 'No Results',
            message: 'No Records Found',
            buttons: ['OK']
          });
          await toast.present();  
    }

    async noResult(){
        const toast = await this.alert.create({
            message: 'No Records Found',
            buttons: ['OK']
          });
          await toast.present();  
    }
    

    async walletCreated(){
      const loading = await this.load.create({
          message: 'Please wait..',
          duration:990
      });

      await loading.present();
      this.store.dispatch(new GetUserOwnProfile())
  }



    async updateToast(){
        const toast = await this.alert.create({
            header: 'Success',
            message: 'Updated',
            buttons: ['OK']
          });
          await toast.present();  
    }

    async promo(){
        const loading = await this.load.create({
            message: 'Please wait..',
            duration:990
        });
        await loading.present();
    }

    async success(){
        const toast = await this.toast.create({
            message: 'Claimed',
            duration:2000
        });
        await toast.present();
    }

    async updateSuccessfully(){
        const loading = await this.load.create({
            message: 'Updating',
            duration:1000
        });
        await loading.present();
    }

    async loadArticles() {
        this.isLoading = true;
        return await this.load.create({
          message: 'Loading Articles... Please Wait',
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

      async loadPartners() {
        this.isLoading = true;
        return await this.load.create({
          message: 'Loading Partners... Please Wait',
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }
      
      
      async loadAd() {
        this.isLoading = true;
        return await this.load.create({
          message: 'Loading Advertisements... Please Wait',
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

      async loadData() {
        this.isLoading = true;
        return await this.load.create({
          message: 'Gathering Data... Please Wait',
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

      async loadEvents() {
        this.isLoading = true;
        return await this.load.create({
          message: 'Gathering Shows & Events... Please Wait',
          duration: 5000,
        }).then(a => {
          a.present().then(() => {
            console.log('presented');
            if (!this.isLoading) {
              a.dismiss().then(() => console.log('abort presenting'));
            }
          });
        });
      }

      
      async dismiss() {
        this.isLoading = false;
        return await this.load.dismiss().then(() => console.log('dismissed'));
      }
}
