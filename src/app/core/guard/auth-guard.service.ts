import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

// NGXS
import { Store } from '@ngxs/store';
import { AuthState } from '@store/auth.state';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {
  }

  canActivate() {
    const token = this.store.selectSnapshot(AuthState.token);
    if (token == 'null' || !token || token == undefined) {
      this.router.navigate(['/login']);
      return false;
    } else {
      return true;
    }
  }
}

