import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from '@services/auth.service';
import { AuthGuard } from '../core/guard/auth-guard.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { environment } from '@environments/environment';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule } from '@ngxs/store';
import { LoginPageModule } from '@pages/login/login.module'
import { NGXS_PLUGINS } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { AuthState } from '@store/auth.state';
import { HttpRequestInterceptor } from '@interceptors/http-request-interceptor';
import { AlertProvider } from '@provider/alert-provider';
import { PostState } from '@store/post/post.state';
import { CountryState } from '@store/countries/countries.state';
import { UserState } from '@store/user/user.state' ;
import { NotificationState } from '@store/notifications/notificatios.state';
import { CategoriesState } from '@store/categories/categories.state';

@NgModule({
    declarations:[],
    imports:[
        CommonModule,
        AppRoutingModule,
        LoginPageModule,
        NgxsStoragePluginModule.forRoot({
            key: ['auth.token','auth.id','auth.image']
        }),
        NgxsModule.forRoot([AuthState]),
        NgxsModule.forFeature([PostState]),
        NgxsModule.forFeature([UserState]),
        NgxsModule.forFeature([CountryState]),
        NgxsModule.forFeature([NotificationState]),
        NgxsModule.forFeature([CategoriesState]),
        NgxsRouterPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot({
        disabled: environment.production
        }),
    ],
    providers:[
        AuthService,
        AuthGuard,
        AlertProvider,
        {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true},
      ]

})

export class CoreModule {}