import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Login } from '../../store/auth.action' ;
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login_form:FormGroup;
  emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  submitButtonStatus = true;
  formSubmitAttempt = false;

  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    private store: Store
  ) {
      this.login_form = this.formBuilder.group({
        email: [null, [Validators.required, Validators.pattern(this.emailRegEx)]],
        password: [null, Validators.required],
      })
   }

 

  ngOnInit() {
  }

  ionViewWillEnter(){
      
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  gotoSiriusLogin(){
    this.store.dispatch(new Navigate(['/sirius-login']))
  }

  onLoginForm(form:FormGroup){
      this.formSubmitAttempt = true;
      if(form.valid){
          this.setSubmitButton(false)
          this.formSubmitAttempt = false
          this.store.dispatch(new Login(form.value)).subscribe(
              () => {
                  this.setSubmitButton(true)
              }, err => {
                  console.log(err)
                  this.setSubmitButton(true);
              }
          )
      }
  }

}
