import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Auth } from '@models/auth.model';
import { AuthState } from '@store/auth.state';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
import { GetUserOwnProfile } from '@store/auth.action';
import { GetOwnPost } from '@store/post/post.action';
import { faEdit,faImage,faClipboard,faVideo } from '@fortawesome/free-solid-svg-icons';
import { environment } from '@environments/environment';
import { Post } from '@models/post.model';
import { Navigate } from '@ngxs/router-plugin';
import { User } from '@models/user.model';
import { ModalController } from '@ionic/angular';
import { UserFollowPage } from '@pages/user/user-follow/user-follow.page';
import { UserFollowingPage } from '@pages/user/user-following/user-following.page';
import { CreateBoardPage } from '@pages/modal/create-board/create-board.page';
import { GetUserBoardOwned } from '@store/user/user.action';
import { Board,BoardStateModel } from '@models/board.model';
import { UserState } from '@store/user/user.state';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';







@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  
  edit = faEdit;
  contentLoaded = false;
  @Select(AuthState.getUserOwnProfile) profile$: Observable<User>
  @Select(PostState.getOwnPosts) post$: Observable<Post[]>
  @Select(UserState.getUserBoardOwned) boards$: Observable<Board[]>
  readonly ENDPOINT_URL = environment.endpoint;
  segments:any;
  faVideo = faVideo;
  faImage = faImage;
  faBoard = faClipboard;
  event:any;


  constructor(
    private store:Store,
    private modalCtrl:ModalController,
    private platform:Platform,
    private storage:Storage
  ) { }

  ngOnInit() {

  }

  async viewFollowers(id:number){
      this.modalCtrl.create({
          component:UserFollowPage,
          componentProps:{
            id:id
         }
      }).then(modal => modal.present())
  }

  async segmentChanged(event){
      const val = event.detail.value
      if( val === 'photos' ){
        this.event = val;
        this.store.dispatch(new GetOwnPost())
      } else if ( val === 'boards' ) {
        this.event = val;
        console.log(this.event);
        this.store.dispatch(new GetUserBoardOwned())
      }
  }

  
  async viewFollowings(id:number){
    this.modalCtrl.create({
        component:UserFollowingPage,
        componentProps:{
          id:id
       }
    }).then(modal => modal.present())
}

  ionViewWillEnter(){
    this.platform.ready().then(() => {
      this.storage.clear();
    });
      this.store.dispatch(new GetUserOwnProfile())
      this.store.dispatch(new GetUserBoardOwned()).subscribe(
          (data) => {
              console.log(data)
          },err => {

          }
      )
      setTimeout(()=>{
        this.contentLoaded = true;
    } , 3000)
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  async createBoard(){
    this.modalCtrl.create({
        component:CreateBoardPage,
    }).then(modal => modal.present())
}

async gotoProfile(){
    this.store.dispatch(new Navigate(['/user-update-profile']))
}

  viewImage(id:number){
    this.store.dispatch(new Navigate([`/post-view/${id}`]))
  }

  async postImage(){
    this.store.dispatch(new Navigate([`/post-image`]))
  }

  async viewBoard(id:number){
    this.store.dispatch(new Navigate([`/board-view/${id}`]))
  }




}

