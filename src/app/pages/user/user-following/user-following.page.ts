import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams } from '@ionic/angular';
import { environment } from '@environments/environment';
import { Store,Select } from '@ngxs/store';
import { User } from '@models/user.model';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';
import { UserState } from '@store/user/user.state';
import { GetFollowing } from '@store/user/user.action';



@Component({
  selector: 'app-user-following',
  templateUrl: './user-following.page.html',
  styleUrls: ['./user-following.page.scss'],
})
export class UserFollowingPage implements OnInit {

  readonly ENDPOINT_URL = environment.endpoint;
  id:number;
  @Select(UserState.getAllFollowing) following$:Observable<User[]>



  constructor(
    private modalCtrl:ModalController,
    private navParams:NavParams,
    private store: Store,
  ) { }

  ngOnInit() {
  }

  
  close(){
    this.modalCtrl.dismiss()
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  async ionViewWillEnter(){
      this.store.dispatch(new GetFollowing(this.id))
  }


}
