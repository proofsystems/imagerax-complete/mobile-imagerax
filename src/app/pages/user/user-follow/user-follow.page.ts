import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams } from '@ionic/angular';
import { environment } from '@environments/environment';

//NGXS
import { Store,Select } from '@ngxs/store';
import { User } from '@models/user.model';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';
import { UserState } from '@store/user/user.state';
import { GetFollowers } from '@store/user/user.action';
import { AuthState } from '@store/auth.state';
import { FollowUser } from '@store/user/user.action';
import {  Auth } from '@models/auth.model';



@Component({
  selector: 'app-user-follow',
  templateUrl: './user-follow.page.html',
  styleUrls: ['./user-follow.page.scss'],
})
export class UserFollowPage implements OnInit {
  readonly ENDPOINT_URL = environment.endpoint;
  id:number;
  @Select(UserState.getAllFollowers) followers$:Observable<User[]>
  userId =  this.store.selectSnapshot(AuthState.getUserId)



  constructor(
    private modalCtrl:ModalController,
    private navParams:NavParams,
    private store: Store,

  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.id = this.navParams.get('id')
      console.log(this.id);
      this.store.dispatch(new GetFollowers(this.id))
  }

  close(){
    this.modalCtrl.dismiss()
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  onFollowUser(id:number){
    this.store.dispatch(new FollowUser(id)).subscribe(
        () => {
            this.store.dispatch(new GetFollowers(this.id))
        }, err =>{
            console.log(err);
            
        }
    )
}

}
