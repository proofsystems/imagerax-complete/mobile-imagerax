import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFollowPage } from './user-follow.page';

describe('UserFollowPage', () => {
  let component: UserFollowPage;
  let fixture: ComponentFixture<UserFollowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFollowPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFollowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
