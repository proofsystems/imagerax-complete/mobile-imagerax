import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { User } from '@models/user.model' ;
import { ViewUserProfile } from '@store/auth.action';
import { Navigate } from '@ngxs/router-plugin';
import { AuthState } from '@store/auth.state';
import { Observable } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { environment } from '@environments/environment';
import { UserFollowPage } from '@pages/user/user-follow/user-follow.page';
import { UserFollowingPage } from '@pages/user/user-following/user-following.page';
import { ModalController } from '@ionic/angular';
import { Post } from '@models/post.model';
import { PostState } from '@store/post/post.state';
import { GetPostImageById } from '@store/post/post.action';
import { FollowUser } from '@store/user/user.action';
import { throttleTime } from 'rxjs/operators';
import { Auth } from '@models/auth.model';




@Component({
  selector: 'app-view-user-profile',
  templateUrl: './view-user-profile.page.html',
  styleUrls: ['./view-user-profile.page.scss'],
})
export class ViewUserProfilePage implements OnInit {
  user_id = this.route.snapshot.params["id"];
  /*
    bindin in HTML
   */
  readonly ENDPOINT_URL = environment.endpoint;
  @Select(AuthState.viewUserProfile) viewProfile$: Observable<User>
  auth_id = this.store.selectSnapshot(AuthState.getUserId)

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private modalCtrl:ModalController
  ) { }

  ngOnInit() {
  }

  async viewFollowers(id:number){
    this.modalCtrl.create({
        component:UserFollowPage,
        componentProps:{
            id:id
        }
    }).then(modal => modal.present())
}

async viewFollowing(id:number){
  this.modalCtrl.create({
    component:UserFollowingPage,
    componentProps:{
        id:id
    }
}).then(modal => modal.present())
}

  async getData(){
    this.store.dispatch(new ViewUserProfile(this.user_id))

  }

  ionViewWillEnter(){
      this.store.dispatch(new ViewUserProfile(this.user_id))
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  onFollowUser(id:number){
      const auth_id = this.store.selectSnapshot(AuthState.getUserId)
      this.store.dispatch(new FollowUser(id)).subscribe(
          () => {
              this.getData()
          }, err =>{
              console.log(err);
              
          }
      )
  }

}
