import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { AuthState } from '@store/auth.state';
import { Observable } from 'rxjs';
import { GetUserOwnProfile } from '@store/auth.action';
import { UpdateProfile,UpdateCoverPhoto } from '@store/user/user.action';
import { environment } from '@environments/environment';
import { Navigate } from '@ngxs/router-plugin';
import { User } from '@models/user.model';
import { FormGroup, FormBuilder, Validators, FormArray, Form, FormControl } from '@angular/forms';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera,CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { LoadingController,ActionSheetController } from '@ionic/angular';

const STORAGE_KEY = 'image';

@Component({
  selector: 'app-user-update-profile',
  templateUrl: './user-update-profile.page.html',
  styleUrls: ['./user-update-profile.page.scss'],
})
export class UserUpdateProfilePage implements OnInit {
  @Select(AuthState.getUserOwnProfile) profile$: Observable<User>
  contentLoaded = false;
  readonly ENDPOINT_URL = environment.endpoint;
  detail_form:FormGroup;
  image:any='';
  imageData:any='';
  pic:any;
  coverImage:any='';
  coverDirectory:any;
  imageDirectory:any;
  fileTransferProfilePic:any;
  coverPhotoFile:any;

  constructor(
    private store:Store,
    private formBuilder:FormBuilder,
    private camera:Camera,
    private file:File,
    private webview:WebView,
    private storage:Storage,
    private platform : Platform,
    private ref:ChangeDetectorRef,
    private filePath:FilePath,
    private transfer:FileTransfer,
    private loadingController:LoadingController,
    private actionSheetController:ActionSheetController
    ) { 

      this.detail_form = this.formBuilder.group({
          f_name:[null],
          l_name:[null],
          email:[null],
          bio:[null]
      })

    }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.coverImage;
    this.platform.ready().then(() => {
      this.storage.clear();
    });
    console.log(this.DoneButton())
    this.store.dispatch(new GetUserOwnProfile()).subscribe( details => {
        this.detail_form.get('f_name').setValue(details['auth'].user.fname);
        this.detail_form.get('l_name').setValue(details['auth'].user.lname);
        this.detail_form.get('email').setValue(details['auth'].user.email);
        this.detail_form.get('bio').setValue(details['auth'].user.description);
    })
      setTimeout(()=>{
        this.contentLoaded = true;
    } , 3000)
  }

  async DoneButton() {
      if(this.coverDirectory == null) {
        return true;
      } else {
          return false;
      }
  }

  async takePictureProfilePic(sourceType:PictureSourceType){
          var options: CameraOptions = {
            quality: 100,
            mediaType: this.camera.MediaType.ALLMEDIA,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
      this.camera.getPicture(options).then(imagePath => {
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
        } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    });
  }

  async takePictureCoverPhoto(sourceType:PictureSourceType){
    var options: CameraOptions = {
      quality: 100,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
  };
this.camera.getPicture(options).then(imagePath => {
  if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
      this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
              this.copyFileToLocalDirCoverPhoto(correctPath, currentName, this.createFileName());
          });
  } else {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDirCoverPhoto(correctPath, currentName, this.createFileName());
  }
  });
}

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.takePictureCoverPhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.takePictureCoverPhoto(this.camera.PictureSourceType.CAMERA);
                }
            },
            {
              text: 'Upload',
              handler: () => {
                  this.startUploadCover()
            }
          },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
  }

    async copyFileToLocalDir(namePath, currentName, newFileName) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
          this.updateStoredImages(newFileName);
      }, error => {
          console.log(error)
      });
    }

    async copyFileToLocalDirCoverPhoto(namePath, currentName, newFileName) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
          this.updateStoredCoverPhoto(newFileName);
      }, error => {
          console.log(error)
      });
    }

    createFileName() {
      var d = new Date(),
          n = d.getTime(),
          newFileName = n + ".jpg";
      return newFileName;
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  
  pathforCoverPhoto(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  async updateStoredCoverPhoto(name) {
    console.log(name)
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
  
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathforCoverPhoto(filePath);
        console.log(filePath);

        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };

        this.coverImage = [newEntry, ...this.coverImage];
        this.coverDirectory = this.coverImage[0].path
        this.ref.detectChanges(); // trigger change detection cycle
        
    });
  }
  

  async updateStoredImages(name) {
    console.log(name)
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
  
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
        console.log(filePath);

        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };

        this.image = [newEntry, ...this.image];
        this.imageDirectory = this.image[0].path
        console.log(this.image)
        this.ref.detectChanges(); // trigger change detection cycle
        
    });
  }
  
  
  async openCam() {
      this.takePictureProfilePic(this.camera.PictureSourceType.CAMERA);
  }

  async openGallery() {
    this.takePictureProfilePic(this.camera.PictureSourceType.PHOTOLIBRARY);
  }


  async onUpdateDetails(){
      const loading = await this.loadingController.create({
        message:'Updating Profile...'
      });
      await loading.present();

  }

  startUpload() {
    let img = this.image[0];
    console.log(img)
    console.log(img.filePath);
    this.file.resolveLocalFilesystemUrl(img.filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => 
              this.readFile(file))
        })
        .catch(err => {
            // this.alertProvider.loginErrorLoader(err)
    });
  }

  startUploadCover() {
    let img = this.coverImage[0];
    console.log(img)
    this.file.resolveLocalFilesystemUrl(img.filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => 
              this.readFileCoverFile(file))
        })
        .catch(err => {
            // this.alertProvider.loginErrorLoader(err)
    });
  }

 async readFileCoverFile (file: any) {
  const reader = new FileReader();
  reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });
      const formData = new FormData();
      formData.append('cover_photo',imgBlob,file.name);
      this.store.dispatch(new UpdateCoverPhoto(formData)).subscribe(
        () => {
            this.storage.clear
        }
    )
    };
  console.log(file);
  reader.readAsArrayBuffer(file);
}


 async readFile(file: any) {
  const reader = new FileReader();
  reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });
      const formData = new FormData();
      formData.append('image',imgBlob,file.name)
      formData.append('fname',this.detail_form.get('f_name').value);
      formData.append('lname',this.detail_form.get('l_name').value);
      formData.append('email',this.detail_form.get('email').value);
      formData.append('description',this.detail_form.get('bio').value);
      this.store.dispatch(new UpdateProfile(formData)).subscribe(
          () => {
              this.storage.clear
          }
      )
     
  };
  reader.readAsArrayBuffer(file);
}




  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

}
