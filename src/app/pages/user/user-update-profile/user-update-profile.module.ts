import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { IonicModule } from '@ionic/angular';

import { UserUpdateProfilePage } from './user-update-profile.page';

const routes: Routes = [
  {
    path: '',
    component: UserUpdateProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LazyLoadImageModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserUpdateProfilePage]
})
export class UserUpdateProfilePageModule {}
