import { Component, OnInit } from '@angular/core';
import { Store,Select, Selector } from '@ngxs/store';
import { Country } from '@models/country.model';
import { Auth } from '@models/auth.model';
import { GetAllCountries }  from '@store/countries/countries.action';
import { CountryState } from '@store/countries/countries.state';
import { Login,SignUp } from '@store/auth.action';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { AuthState } from '@store/auth.state';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { subscribeOn } from 'rxjs/operators';




@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  @Select(CountryState.getAllCountries) countries$: Observable<Country[]>
  emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
  passwordType: string = 'password';
  showPass = false;
  register_form:FormGroup;
  submitButtonStatus = true;
  formSubmitAttempt = false;



  constructor(
    private store:Store,
    private formBuilder: FormBuilder,


  ) {
      this.register_form = this.formBuilder.group({
            fname: ['',[Validators.required,Validators.minLength(1)]],
            lname: ['',[Validators.required,Validators.minLength(1)]],
            email:  ['',[Validators.required,Validators.pattern(this.emailRegEx)]],
            password: [null,Validators.required],
            confirm_password:  [null,Validators.required],
            country:  [null,Validators.required],
      })
   }

  ngOnInit() {

  }

  async ionViewWillEnter(){
      this.store.dispatch(new GetAllCountries())
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }


  showPassword(){
    this.showPass = !this.showPass
    if(this.showPass){
          this.passwordType = 'text';
    } else {
        this.passwordType = 'password';
    }
}


  onRegister( form:FormGroup){
      this.formSubmitAttempt = true
      if(form.valid){
          this.store.dispatch(new SignUp(form.value)).subscribe(
              () => {
                  this.setSubmitButton(true)
                  this.register_form.reset()
              } , err => {
                  this.setSubmitButton(true)
              }
          )
      }
  }



}
