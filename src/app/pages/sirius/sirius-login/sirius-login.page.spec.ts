import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiriusLoginPage } from './sirius-login.page';

describe('SiriusLoginPage', () => {
  let component: SiriusLoginPage;
  let fixture: ComponentFixture<SiriusLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiriusLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiriusLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
