import { Component, OnInit } from '@angular/core';
import { LoginRequestMessage, VerifytoLogin, VerifytoConfirmCredential, ApiNode } from 'siriusid-sdk';


@Component({
  selector: 'app-sirius-login',
  templateUrl: './sirius-login.page.html',
  styleUrls: ['./sirius-login.page.scss'],
})
export class SiriusLoginPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
