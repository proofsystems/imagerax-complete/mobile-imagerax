import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { GetAllCategories,SelectCategory } from '@store/categories/categories.action';
import { CategoriesState } from '@store/categories/categories.state';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Categories } from '@models/categories.model';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ModalController,NavParams } from '@ionic/angular';


@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.page.html',
  styleUrls: ['./select-category.page.scss'],
})

export class SelectCategoryPage implements OnInit {
  cat_form:FormGroup;
  @Select(CategoriesState.getAllCategories) categories$: Observable<Categories[]>

  constructor(private store:Store,private formBuilder: FormBuilder,private modalCtrl:ModalController) {
      this.cat_form = this.formBuilder.group({
          ids:this.formBuilder.array([])
      })
   }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.store.dispatch(new GetAllCategories());
    
  }

 
  
  onCheckboxChange(e) {
    const ids: FormArray = this.cat_form.get('ids') as FormArray;
    console.log(e);
    if (e.target.checked) {
      ids.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      ids.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          ids.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  
  close(){
    this.modalCtrl.dismiss()
  }

  onSubmit() {
    this.store.dispatch(new SelectCategory(this.cat_form.value)).subscribe(
      response => {
        this.close();
      },
      err => {
          console.log('error')
      }
    );
  }


}
