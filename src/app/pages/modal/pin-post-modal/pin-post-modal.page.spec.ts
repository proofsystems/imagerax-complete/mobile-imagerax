import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinPostModalPage } from './pin-post-modal.page';

describe('PinPostModalPage', () => {
  let component: PinPostModalPage;
  let fixture: ComponentFixture<PinPostModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinPostModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinPostModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
