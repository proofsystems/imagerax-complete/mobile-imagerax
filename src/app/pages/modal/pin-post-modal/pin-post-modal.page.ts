import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams } from '@ionic/angular';
import { environment } from '@environments/environment';
import { Store,Select } from '@ngxs/store';
import { Post,PostStateModel } from '@models/post.model';
import { GetPostImageById,GetAllBoards,PinPost } from '@store/post/post.action';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
import { Board, BoardStateModel } from '@models/board.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthState } from '@store/auth.state';



@Component({
  selector: 'app-pin-post-modal',
  templateUrl: './pin-post-modal.page.html',
  styleUrls: ['./pin-post-modal.page.scss'],
})
export class PinPostModalPage implements OnInit {

  id:number;
  readonly ENDPOINT_URL = environment.endpoint;
  @Select(PostState.getPostImageById) post$: Observable<Post>
  @Select(PostState.getAllBoards) boards$:Observable<Board[]>
  submitButtonStatus = true;
  formSubmitAttempt = false;
  pin_form:FormGroup;
  userId =  this.store.selectSnapshot(AuthState.getUserId);
  allBoard:Board[];


  constructor(

    private modalCtrl:ModalController,
    private navParams:NavParams,
    private store: Store,
    private formBuilder:FormBuilder,


  ) 
   { 
      this.pin_form = this.formBuilder.group({
          board_id:['',[Validators.required,Validators.minLength(1)]],
          post_id:[null]
      })
   }

  ngOnInit() {
  }

  async ionViewWillEnter(){
      this.id = this.navParams.get('id')
      this.store.dispatch(new GetPostImageById(this.id))
      this.store.dispatch(new GetAllBoards()).subscribe( board => {
          let ownBoard = board.post.boards.filter( b => b.owned_by.id === this.userId );
          this.allBoard = ownBoard;
      })
      this.pin_form.get('post_id').setValue(this.id)
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  close(){
      this.modalCtrl.dismiss()
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  onPinPost(form:FormGroup){
    console.log(form.value)
    this.formSubmitAttempt = true;
      if(form.valid){
          this.setSubmitButton(false)
          this.formSubmitAttempt = false
          this.store.dispatch(new PinPost(form.value)).subscribe(
              () => {
                  this.setSubmitButton(true)
                  this.pin_form.reset()
                  this.close()
              }, err => {
                  console.log(err)
                  this.setSubmitButton(true);
              }
          )
      }
  }

}
