import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBoardPage } from './create-board.page';

describe('CreateBoardPage', () => {
  let component: CreateBoardPage;
  let fixture: ComponentFixture<CreateBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBoardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
