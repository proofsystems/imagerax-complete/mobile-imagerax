import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams } from '@ionic/angular';
import { environment } from '@environments/environment';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { Store } from '@ngxs/store';
import { CreateBoard } from '@store/post/post.action'
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-board',
  templateUrl: './create-board.page.html',
  styleUrls: ['./create-board.page.scss'],
})
export class CreateBoardPage implements OnInit {

create_form:FormGroup;
submitButtonStatus = true;
formSubmitAttempt = false;


  constructor(
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private router:Router,
    private store: Store
  ) 
  { 
    this.create_form = this.formBuilder.group({
      name: [null, Validators.required],
      visibility: [null, Validators.required],
      category: [null, Validators.required]
    })
  }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss()
  }

  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

  onCreateBoard(form:FormGroup){
      this.formSubmitAttempt = true;
      if(form.valid){
          this.setSubmitButton(false)
          this.formSubmitAttempt = false
          this.store.dispatch(new CreateBoard(form.value)).subscribe(
              () => {
                this.create_form.reset()
                this.setSubmitButton(true)
                // this.close()
              }, err => {
                  console.log(err)
                  this.setSubmitButton(true)
              }
          )
      }
  }

}
