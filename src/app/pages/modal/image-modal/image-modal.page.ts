import { Component, OnInit } from '@angular/core';
import { ModalController,NavParams } from '@ionic/angular';
import { environment } from '@environments/environment';


@Component({
  selector: 'app-image-modal',
  templateUrl: './image-modal.page.html',
  styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage implements OnInit {
  sliderOpts:any;
  img:string;
  readonly ENDPOINT_URL = environment.endpoint;


  constructor(
    private modalCtrl:ModalController,
    private navParams:NavParams
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.img = this.navParams.get('id');
      console.log(this.img)
  }

  close(){
    this.modalCtrl.dismiss()
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

}
