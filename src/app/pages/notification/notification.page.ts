import { Component, OnInit } from '@angular/core';
import { GetAllNotifications,GetRecentNotifications } from '@store/notifications/notifications.action';
import { throwError, Observable } from 'rxjs';
import { Navigate } from '@ngxs/router-plugin';
import { Notification,NotificationStateModel } from '@models/notification.model';
import { Store,Select } from '@ngxs/store';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { NotificationState } from '@store/notifications/notificatios.state';





@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  readonly ENDPOINT_URL = environment.endpoint;
  @Select(NotificationState.getAllNotification) notifications:Observable<Notification[]>

  constructor(
    private router:Router,
    private store: Store
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.store.dispatch(new GetAllNotifications())
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  gotoNotificaion

}
