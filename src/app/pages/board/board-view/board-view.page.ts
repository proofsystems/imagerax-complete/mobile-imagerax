import { Component, OnInit } from '@angular/core';
import { GetUserBoardById } from '@store/user/user.action';
import { UserState } from '@store/user/user.state';
import { Board } from '@models/board.model';
import { Navigate } from '@ngxs/router-plugin';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { AuthState } from '@store/auth.state';
import { faArrowLeft,faStar,faShare,faEllipsisH,faMapPin,faPaperPlane,faHeart,faEye } from '@fortawesome/free-solid-svg-icons';





@Component({
  selector: 'app-board-view',
  templateUrl: './board-view.page.html',
  styleUrls: ['./board-view.page.scss'],
})
export class BoardViewPage implements OnInit {

  boardId = this.route.snapshot.params["id"];
  readonly ENDPOINT_URL = environment.endpoint
  @Select(UserState.getUserBoardById) board$: Observable<Board>
  star = faStar
  share = faShare
  more = faEllipsisH
  pin = faMapPin
  send = faPaperPlane
  heart = faHeart
  eye = faEye
  image = this.store.selectSnapshot(AuthState.getProfile)



  constructor(
    private route:ActivatedRoute,
    private store:Store,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.store.dispatch(new GetUserBoardById(this.boardId))
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  viewImage(id:number){
    this.store.dispatch(new Navigate([`/post-view/${id}`]))
  }


}
