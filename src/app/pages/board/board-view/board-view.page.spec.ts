import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardViewPage } from './board-view.page';

describe('BoardViewPage', () => {
  let component: BoardViewPage;
  let fixture: ComponentFixture<BoardViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
