import { Component } from '@angular/core';
import { faHome,faSearch,faComment,faUser } from '@fortawesome/free-solid-svg-icons';
import { Navigate } from '@ngxs/router-plugin';
import { State,Action,StateContext,Selector,Store } from '@ngxs/store';



@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  home = faHome
  search = faSearch
  comment = faComment
  user = faUser


  constructor(
      private store:Store
  ) {}

    gotoUserProfile(){
      this.store.dispatch(new Navigate(['/user-profile']))
    }

    gotoSearch(){
      this.store.dispatch(new Navigate(['/search']))
    }

   

}
