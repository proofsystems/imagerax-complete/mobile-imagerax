import { Component, OnInit,ElementRef,ViewChild } from '@angular/core'; 
import { ProximaxService } from '@services/proximax.service';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { AlertProvider } from '@provider/alert-provider';
import { throttleTime } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { GetUserOwnProfile } from '@store/auth.action';
import * as Bounce from 'bounce.js';



@Component({
  selector: 'app-create-wallet',
  templateUrl: './create-wallet.page.html',
  styleUrls: ['./create-wallet.page.scss'],
})
export class CreateWalletPage implements OnInit {
  
  @ViewChild('walletImg', { read:ElementRef, static:true })walletImg: ElementRef;
  form:FormGroup;
   submitButtonStatus = true;
   formSubmitAttempt = false;
   hasAccount: boolean = false;
   noAccount: boolean = false;


  constructor(
      private proximax:ProximaxService,
      private formBuilder: FormBuilder,
      private router: Router,
      private alertProvider:AlertProvider,
      private store:Store

  ) {
    this.form = this.formBuilder.group({
      wallet_name: [null, Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
    })
   
   }

  ngOnInit() {
  }

  get password(){
      return this.form.get('password')
  }

  ionViewWillEnter(){
      this.bounce()
      this.store.dispatch(new GetUserOwnProfile()).subscribe( user => {
          console.log('-----Profile Wallet Shit-----' , user)

          let _addr = user.auth.user.wallet_address
          let _pub = user.auth.user.wallet_public_key;

          if (this.proximax.getAccountInfo(_addr) == true) {
            this.hasAccount = false;
            this.noAccount = true;
          } else {
            this.hasAccount = true;
            this.noAccount = false;
          }

      })
  }


  bounce() {
    var bounce = new Bounce();
    bounce
      .translate({
        from: { x: -300, y: 0 },
        to: { x: 0, y: 0 },
        duration: 600,
        stiffness: 4
      })
      .scale({
        from: { x: 1, y: 1 },
        to: { x: 0.1, y: 2.3 },
        easing: "sway",
        duration: 800,
        delay: 65,
        stiffness: 2
      })
      .scale({
        from: { x: 1, y: 1 },
        to: { x: 5, y: 1 },
        easing: "sway",
        duration: 500,
        delay: 30,
      })
      .applyTo(this.walletImg.nativeElement);
  }

 
  getSubmitButton() {
    return this.submitButtonStatus;
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }


  async createWallet(form:FormGroup){
      const name = form.value.wallet_name;
      const password = form.value.password;
      if(form.valid){
          this.setSubmitButton(false)
          this.formSubmitAttempt = false
          this.proximax.createWallet(name,password)
          this.alertProvider.walletCreated()
          this.form.reset()
          this.store.dispatch(new Navigate(['/tabs']))
      }
  }
}