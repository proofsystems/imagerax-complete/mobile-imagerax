import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Post } from '@models/post.model';
import { GetAllTrends,GetTrendsToday,GetAllFavorites,GetFavoritesToday, SearchTrends } from '@store/post/post.action';
import { Navigate } from '@ngxs/router-plugin';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { Trends } from '@models/trends.model';
import { Yours } from '@models/yours.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { debounce, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  searchForm: FormGroup;
  segments:any;
  readonly ENDPOINT_URL = environment.endpoint;
  @Select(PostState.getAllTrends) data$: Observable<Trends[]>;
  contentLoaded = false;
  constructor(
    private store:Store,private fb: FormBuilder,

  ) {
    this.searchForm = this.fb.group({
      caption: ['', [Validators.required]]
    });
    this.searchForm
    .get('caption').valueChanges.pipe(debounceTime(500))
    .subscribe((caption:string)=>{
        this.store.dispatch(new SearchTrends({val:caption}))
    })
   }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.segments = "trends";
    setTimeout(()=>{
      this.contentLoaded = true;
    } , 3000)
  }

  async segmentChanged(event){
      const value = event.detail.value;
      if (value === 'trends'){
        this.store.dispatch(new GetAllTrends())
      } else if(value == 'favorites'){
        this.store.dispatch(new GetAllFavorites())
      }
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }
}
