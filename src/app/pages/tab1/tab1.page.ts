import { Component } from '@angular/core';
//NGXS
import { Store, Select } from '@ngxs/store';
import { Post } from '@models/post.model';
import { GetAllPost, GetAllFeeds,GetPostsPerPage,SearchTrends } from '@store/post/post.action';
import { Navigate } from '@ngxs/router-plugin';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
//env
import { environment } from '@environments/environment';
import { faBell,faSignOutAlt,faPlus,faComment,faStar,faDownload } from '@fortawesome/free-solid-svg-icons';
import { GetAllCountries } from '@store/countries/countries.action';
import { CountryState } from '@store/countries/countries.state';
import { Country,CountryStateModel } from '@models/country.model';
import { Router,ActivatedRoute } from '@angular/router';
import { ImageModalPage } from '@pages/modal/image-modal/image-modal.page';
import { SelectCategoryPage } from '@pages/modal/select-category/select-category.page';
import { CreateBoardPage } from '@pages/modal/create-board/create-board.page';
import { AlertController,ModalController } from '@ionic/angular';
import { Feeds  } from '@models/feeds.model';
import { GetUserOwnProfile } from '@store/auth.action';
import { ProximaxService } from '@services/proximax.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { debounce, debounceTime } from 'rxjs/operators';



@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  faBell = faBell;
  faSignOutAlt = faSignOutAlt;
  faUpload = faPlus;
  comment = faComment;
  star = faStar;
  segments:any;
  download = faDownload;
  contentLoaded = false;
  key:string;
  page: string;
  hasAccount: boolean = false;
  noAccount: boolean = false;
  searchForm: FormGroup;

  readonly ENDPOINT_URL = environment.endpoint;
  @Select(PostState.getAllPost) post$: Observable<Post[]>
  @Select(PostState.getAllFeeds) feed$: Observable<Feeds[]>
  @Select(CountryState.getAllCountries) countries$: Observable<Country[]>
  //NGXS

  constructor(
    private store: Store,
    private modalCtrl:ModalController,
    private alertCtrl:AlertController,
    private proximax:ProximaxService,
    private fb:FormBuilder
  ) {
    
    this.searchForm = this.fb.group({
      caption: ['', [Validators.required]]
    });
    this.searchForm
    .get('caption').valueChanges.pipe(debounceTime(500))
    .subscribe((caption:string)=>{
        this.store.dispatch(new SearchTrends({val:caption}))
    })

  }
  
  async ionViewWillEnter(){
    this.segments = "me";
    setTimeout(()=>{
      this.contentLoaded = true;
    } , 5000)
    this.store.dispatch(new GetAllPost());
    this.store.dispatch(new GetAllFeeds());
    this.store.dispatch(new GetUserOwnProfile()).subscribe( user => {
      console.log('-----Profile Wallet Shit-----' , user)
      console.log(user)
      let _addr = user.auth.user.wallet_address
      let _pub = user.auth.user.wallet_public_key
     
      if (this.proximax.getAccountInfo(_addr) == true) {
        this.hasAccount = false;
        this.noAccount = true;
      } else {
        this.hasAccount = true;
        this.noAccount = false;
      }
      if(user.auth.user.categories.length === 0){
        this.openModalCategory();
      } else {
        console.log('error')
      }
  })
  }

  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

  async openModalCategory(){
    this.modalCtrl.create({
      component:SelectCategoryPage,
  }).then(modal => modal.present())
  }
  
  viewImage(id:number){
    this.store.dispatch(new Navigate([`/post-view/${id}`]))
  }

  gotoNotification(){
    this.store.dispatch(new Navigate([`/notification`]))
  }

  async openImagemodal(id:number){
    this.modalCtrl.create({
      component:ImageModalPage,
      componentProps:{
          id:id
      }
    }).then(modal => modal.present())
}

async createBoard(){
    this.modalCtrl.create({
        component:CreateBoardPage,
    }).then(modal => modal.present())
}

async loadData(event) {
  setTimeout(() => {
    console.log('Done');
    event.target.complete();
    //App logic
   
  }, 500);
}

async postImage(){
  this.store.dispatch(new Navigate([`/post-image`]))
}

async postVideo(){
    this.store.dispatch(new Navigate([`/post-video`]))
}

async logout(){
    localStorage.clear()
}

async logoutModal(){
  const alert = await this.alertCtrl.create({
    header: 'Logout',
    message: 'Are you Sure you want to  logout ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        handler: () => {
            this.logout
            this.store.dispatch(new Navigate([`/login`]))
        }
      }
    ]
    
  })
  await alert.present();
}

}
