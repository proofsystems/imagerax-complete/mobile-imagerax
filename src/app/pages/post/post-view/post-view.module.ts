import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PostViewPage } from './post-view.page';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertProvider } from '@provider/alert-provider';
import { PipeModule } from '@pipes/pipe.module';




const routes: Routes = [
  {
    path: '',
    component: PostViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    FontAwesomeModule,
    PipeModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [PostViewPage],
  providers:[AlertProvider]
})
export class PostViewPageModule {}
