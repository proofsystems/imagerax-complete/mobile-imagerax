import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
//NGXS
import { Store, Select } from '@ngxs/store';
import { Post } from '@models/post.model';
import { User } from '@models/user.model';
import { GetPostById, CommentPost, LikePost, DeletePost } from '@store/post/post.action';
import { Navigate } from '@ngxs/router-plugin';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthState } from '@store/auth.state';
import { AlertController,ModalController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AlertProvider } from '@provider/alert-provider';



//services
import { AuthService } from '@services/auth.service';

//modals
import { ImageModalPage } from '@pages/modal/image-modal/image-modal.page';
import { PinPostModalPage } from '@pages/modal/pin-post-modal/pin-post-modal.page';
//env
import { environment } from '@environments/environment';
import { PostStateModel } from '@models/post.model';
import { Router,ActivatedRoute } from '@angular/router';
import { faArrowLeft,faStar,faShare,faEllipsisH,faMapPin,faPaperPlane,faHeart,faEye } from '@fortawesome/free-solid-svg-icons';
import {IonContent} from '@ionic/angular';




@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.page.html',
  styleUrls: ['./post-view.page.scss'],
})
export class PostViewPage implements OnInit {
  @ViewChild(IonContent,{static:true}) content:IonContent
  comment_form: FormGroup;
  readonly ENDPOINT_URL = environment.endpoint;
  @Select(PostState.getPostById) post$: Observable<Post>
  postId = this.route.snapshot.params["id"];
  star = faStar
  share = faShare
  more = faEllipsisH
  pin = faMapPin
  send = faPaperPlane
  heart = faHeart
  eye = faEye
  contentLoaded = false;
  submitButtonStatus = true;
  formSubmitAttempt = false;
  //Get From LocalStorage
  userId =  this.store.selectSnapshot(AuthState.getUserId)
  image = this.store.selectSnapshot(AuthState.getProfile)


  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    public authService:AuthService,
    private formBuilder:FormBuilder,
    private alertCtrl: AlertController,
    private modalCtrl:ModalController,
    private socialSharing:SocialSharing,
    private alertProvider:AlertProvider
  ) 
     {
        this.comment_form = this.formBuilder.group({
            post_id:[null],
            comment: ['',[Validators.required,Validators.minLength(1)]],
        })
     }

  ngOnInit() {
    
  }

  async scrollBottom(){
      this.content.scrollToBottom(1500);
  }

  setSubmitButton(flag: boolean) {
    this.submitButtonStatus = flag;
  }

 ionViewWillEnter(){
      this.userId
      this.store.dispatch(new GetPostById(this.postId))
      setTimeout(()=>{
        this.contentLoaded = true;
    } , 3000)
    this.comment_form.get('post_id').setValue(this.postId)
      setTimeout(()=>{
        this.contentLoaded = true;
    } , 3000)
  }

  async likeComment(){
      console.log('aye aye')
  }

  socialShare(image:string){
      this.socialSharing.shareViaFacebook(this.decode(image)).then(()=>{

      }).catch(() =>{

      })
  }

  async viewImage(id:number){
      this.modalCtrl.create({
        component:ImageModalPage,
        componentProps:{
            id:id
        }
      }).then(modal => modal.present())
  }

  async pinModal(id:Post){
    this.modalCtrl.create({
        component:PinPostModalPage,
        componentProps:{
            id:id
        }
    }).then(modal => modal.present())
  }
  
  async getPostById(){
    this.store.dispatch(new GetPostById(this.postId))
  }

  async onCommentPost(form:FormGroup){
          this.store.dispatch(new CommentPost (form.value)).subscribe(
              ()  => {
                    this.scrollBottom();
                    this.getPostById();
                    this.comment_form.get('comment').reset();
              }, err => {
                  console.log(err)
                  this.setSubmitButton(true)
              }
          )
  
  }

  async onLikePost(data:Post){
      this.store.dispatch(new LikePost(data)).subscribe(
          () => {
            this.store.dispatch(new GetPostById(this.postId))
          }, err => {
              console.log(err);
          }
      )
  }

  async onDeletePost(data:Post){
      this.store.dispatch(new DeletePost(data)).subscribe(
          () => {
              this.alertProvider.deletePost(data)
          }, err => {
              console.log(err);
          }
      )
  }

  async viewProfile(id){
    this.store.dispatch(new Navigate([`/view-user-profile/${id}`]))
  }

  async deletConfirmation(data:Post){
    const alert = await this.alertCtrl.create({
      header: 'Delete',
      message: 'Are you Sure you want to delete this post ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
              this.onDeletePost(data)
          }
        }
      ]
      
    })
    await alert.present();
  }


  public decode(image: string) {
    return `${this.ENDPOINT_URL}${decodeURI(image)}`;
  }

 findEqual(likes: Post[]){
      const isLike = likes.some(m => m.user_id == this.userId );
      if(!isLike){
          return false;
      } else {
          return true;
      }
  }
}
