import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AlertProvider } from '@provider/alert-provider'
import { IonicModule } from '@ionic/angular';

import { PostImagePage } from './post-image.page';

const routes: Routes = [
  {
    path: '',
    component: PostImagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    LazyLoadImageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PostImagePage],
  providers:[ AlertProvider ]
})
export class PostImagePageModule {}
