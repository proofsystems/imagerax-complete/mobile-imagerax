import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { CreateBoard } from '@store/post/post.action';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { Store,Select } from '@ngxs/store';
import { Board, BoardStateModel } from '@models/board.model';
import { GetPostImageById,GetAllBoards,PinPost,CreatePost } from '@store/post/post.action';
import { AuthState } from '@store/auth.state'
import { Post,PostStateModel } from '@models/post.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostState } from '@store/post/post.state';
import { Observable } from 'rxjs';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Storage } from '@ionic/storage' ;
import { finalize } from 'rxjs/operators';
import { Camera,CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { Navigate } from '@ngxs/router-plugin';
import { ActionSheetController, ToastController, Platform, LoadingController,ModalController } from '@ionic/angular';
import { AlertProvider } from '@provider/alert-provider';
import { CreateBoardPage } from '@pages/modal/create-board/create-board.page';

const STORAGE_KEY = 'image';


@Component({
  selector: 'app-post-image',
  templateUrl: './post-image.page.html',
  styleUrls: ['./post-image.page.scss'],
})
export class PostImagePage implements OnInit {

  
  post_form:FormGroup
  @Select(PostState.getAllBoards) boards$:Observable<Board[]>
  userId =  this.store.selectSnapshot(AuthState.getUserId)
  images:any = '';
  imageDirectory:any;

  constructor(
    private router:Router,
    private store: Store,
    private camera: Camera,
    private file: File,
    private webview: WebView,
    private storage: Storage,
    private platform: Platform,
    private ref: ChangeDetectorRef, 
    private filePath: FilePath,
    private formBuilder:FormBuilder,
    private alertProvider:AlertProvider,
    private modalCtrl:ModalController
  ) {
    this.store.dispatch(new GetAllBoards())
    this.platform.ready().then(() => {
      this.storage.clear();
    });
      this.post_form = this.formBuilder.group({
        caption: [null, ],
        board_id: [null],
      })
      this.imageDirectory;
      if(this.imageDirectory != null){
          this.alertProvider.sliderNotification();
      }
   }

  ngOnInit() {
  }

  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      console.log(this.images)
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
          console.log(this.images)
        }
      }
    });
  }

  async createBoard(){
    this.modalCtrl.create({
        component:CreateBoardPage,
    }).then(modal => modal.present())
}

  async openCamera(){
    this.takePicture(this.camera.PictureSourceType.CAMERA);
  }

  async openGallery(){
    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  takePicture(sourceType: PictureSourceType) {
    var options: CameraOptions = {
        quality: 100,
        mediaType: this.camera.MediaType.ALLMEDIA,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
    };
    this.camera.getPicture(options).then(imagePath => {
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
        } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
    });
  
  }

  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
}


  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        // this.presentToast('Error while storing file.');
    });
  }
  
  
  startUpload() {
    let img = this.images[0];
    this.file.resolveLocalFilesystemUrl(img.filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => 
              this.readFile(file))
        })
        .catch(err => {
            // this.alertProvider.loginErrorLoader(err)
    });
  }
  
  readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
         console.log(file);
         const formData = new FormData();
        formData.append('image', imgBlob, file.name);
        formData.append('caption', this.post_form.get('caption').value);
        formData.append('board_id', this.post_form.get('board_id').value);
        this.store.dispatch(new CreatePost(formData)).subscribe(
            () => {
                this.images = null;
                this.post_form.reset();
                this.storage.clear();
                // this.store.dispatch(new Navigate(['/tabs']))
            }, err => {
                console.log(err)
            }
        )
           
    };
    reader.readAsArrayBuffer(file);
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
  
  
  
  updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
  
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
        console.log(filePath);
  
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
  
        this.images = [newEntry, ...this.images];
        this.imageDirectory = this.images[0].path;
        this.ref.detectChanges();
        console.log(this.images)
    });
  }



}
