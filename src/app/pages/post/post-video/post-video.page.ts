import { Component, OnInit } from '@angular/core';
import { Camera,CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File,FileEntry } from '@ionic-native/file/ngx';
import { FileTransfer,FileTransferObject,FileUploadOptions } from '@ionic-native/file-transfer/ngx';
import { NavController, AlertController, LoadingController} from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostState } from '@store/post/post.state';
import { Board, BoardStateModel } from '@models/board.model';
import { Store,Select } from '@ngxs/store';
import { GetPostImageById,GetAllBoards,PinPost,CreatePost } from '@store/post/post.action';
import { Observable } from 'rxjs';


const baseUrl = "https://example.com";
const MAX_FILE_SIZE = 5 * 1024 * 1024;
const ALLOWED_MIME_TYPE = "video/mp4";

@Component({
  selector: 'app-post-video',
  templateUrl: './post-video.page.html',
  styleUrls: ['./post-video.page.scss'],
})
export class PostVideoPage implements OnInit {

  selectedVideo: string;
  uploadedVideo: string;
  displayVideo:string;
  post_vid:FormGroup;
  isUploading: boolean = false;
  uploadPercent: number = 0;
  videoFileUpload: FileTransferObject;
  video:any;
  @Select(PostState.getAllBoards) boards$:Observable<Board[]>


  constructor(
    private camera: Camera,
    private transfer: FileTransfer, 
    private file: File,
    private loadingCtrl:LoadingController,
    private alertCtrl:AlertController,
    private webview:WebView,
    private store: Store,
    private formBuilder:FormBuilder,


  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
      this.store.dispatch(new GetAllBoards())
      this.post_vid = this.formBuilder.group({
        board_id:['',[Validators.required,Validators.minLength(1)]],
        caption: ['',[Validators.required,Validators.minLength(1)]],
      })
  }

 async showLoader() {
    const load = await this.loadingCtrl.create({
      message: 'Test',
      duration: 900
    });
    await load.present();
  }

  dismissLoader() {
    
  }

  async presentAlert(title, message) {
    const toast = await this.alertCtrl.create({
        message:'Uploaded',
        buttons: ['OK']
    });
    await toast.present();
  }


  selectVideo() {
    const options: CameraOptions = {
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options)
      .then( async (videoUrl) => {
        if (videoUrl) {
          this.showLoader();
          this.uploadedVideo = null;
          var filename = videoUrl.substr(videoUrl.lastIndexOf('/') + 1);
          var dirpath = videoUrl.substr(0, videoUrl.lastIndexOf('/') + 1);
          dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
          this.selectedVideo = this.webview.convertFileSrc(videoUrl)
          try {
            var dirUrl = await this.file.resolveDirectoryUrl(dirpath);
            var retrievedFile = await this.file.getFile(dirUrl, filename, {});
          } catch(err) {
            this.dismissLoader();
            return this.presentAlert("Error","Something went wrong.");
          }
          
          retrievedFile.file( data => {
              this.dismissLoader();
              if (data.size > MAX_FILE_SIZE) return this.presentAlert("Error", "You cannot upload more than 5mb.");
              if (data.type !== ALLOWED_MIME_TYPE) return this.presentAlert("Error", "Incorrect file type.");
              this.selectedVideo = retrievedFile.nativeURL;
          });
        }
      },
      (err) => {
        console.log(err);
      });
  }

  uploadVideo() {
    var url = baseUrl +  "/video/upload";
    var filename = this.selectedVideo.substr(this.selectedVideo.lastIndexOf('/') + 1);
        var options: FileUploadOptions = {
          fileName: filename,
          fileKey: "video",
          mimeType: "video/mp4",
        }
        const formData = new FormData
    }
}
