export class Comment {
    id: number;
    post_id: number;
    comment: string;
  
    constructor(comment: Comment) {
      this.id = comment.id;
      this.post_id = comment.post_id;
      this.comment = comment.comment;
    }
  }
  
  export class CommentStateModel {
    comments: Comment[];
  }