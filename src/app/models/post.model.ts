import { Comment } from './comment.model';

export interface Post {
  id: number;
  caption: string;
  comments: Comment;
  image: any;
  likes: [];
  user_id: number;
  type: string;
  updated_at:Date;
  board_id:number;

  post_by: {
      id:number;
      email:string;
      fname:string;
      image:string;
      is_followed:number;
      following_count:number;
      updated_at:Date;
  }

}

export class PostStateModel {
  posts: Post[];
  post: Post;
  own_post:Post[];
  posts_feed:Post[];
  totalPages: number;
  itemsPerPage: number;
  currentPage: number;
  searchText: string;
}
