export interface Categories {
    id: number;
    name: string;
    slug: string;
  }
  
  export class CategoriesStateModel {
    cat: Categories;
    cats: Categories[];
  }
  