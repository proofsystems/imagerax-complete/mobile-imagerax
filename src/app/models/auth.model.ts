export interface Auth {
    email: string;
    password: string;
  
    profile: {
      id: any;
      fname: string;
      lname: string;
      cover_photo: string;
      image:string;
      linked_social_accounts: {
        id: number;
        provider_id: string;
        provider_name: string;
        user_id: string;
      };
      wallet_address:string; 
      wallet_public_key:string;
      wallet:string;
    };
  
    token: {
      access_token: string;
    };
  
    fname: string;
    lname: string;
    country_id: number;
  }
  
  export interface SocialAuth {
    access_token: string;
    provider: string;
  }
  
  export class AuthStateModel {
    token: string;
    id: number;
    coverPhoto: string;
    provider: string;
    user:Auth;
    image:string;
    // provider:string
  }
  