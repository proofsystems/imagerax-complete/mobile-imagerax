export interface User {
    id:number;
    email: string;
    password: string;
    followers_count:string;
    following_count:string;
    description:string;
    cover_photo: string;
    image:string;
    fname: string;
    lname: string;
    is_followed:number;
    linked_social_accounts: {
      id: number;
      provider_id: string;
      provider_name: string;
      user_id: string;
    };
  };
    
  
  export class UserStateModel {
    user:User;
    users:User[];
  }
  