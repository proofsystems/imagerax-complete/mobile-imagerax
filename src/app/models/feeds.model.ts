import { Comment } from './comment.model';

export interface Feeds {
  id: number;
  caption: string;
  comments: Comment;
  image: any;
  likes: [];
  user_id: number;
  type: string;
  updated_at:Date;
  board_id:number;

  post_by: {
      id:number;
      email:string;
      fname:string;
      image:string;
      is_followed:number;
      following_count:number;
      updated_at:Date;
  }

}

export class FeedsStateModel {
  feeds: Feeds[];
  feed: Feeds;
}
