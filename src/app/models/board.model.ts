import { Post } from '@models/post.model';

export  interface Board {
    name:string;
    visibility:number;
    id:string;
    owned_by:{
        id:number;
        fname:string;
        lname:string;
        description:string;
        email:string;
        image:string;
        cover_photo:string;
        country_id:string;
        type:string;
        social_account:string;
        num_post:string;
        is_followerd:string;
        country:string;
        image_adress:string;
    }

    posts:Post;

}

export class BoardStateModel {
    board:Board;
    post:Post[];
    boards:Board[];
}