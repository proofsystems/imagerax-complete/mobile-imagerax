import { CountryService } from '@services/countries.service';
import { Country,CountryStateModel } from '@models/country.model';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { GetAllCountries } from '@store/countries/countries.action' ;

@State<CountryStateModel>({
    name:'country',
    defaults:{
        countries:[]
    }
})

export class CountryState {
    @Selector()
    static getAllCountries(state: CountryStateModel){
        return state.countries
    }
    
    constructor(private countryService: CountryService, private store: Store){

    }

    @Action(GetAllCountries)
    getAllCountries({patchState}: StateContext<CountryStateModel>,{}:GetAllCountries){
        return this.countryService.getAll().pipe(
            tap((result : Country[])=>{
                patchState({
                    countries:result
                })
                console.log(result)
            }),
            catchError(err => {
                return throwError(err);
              })
        )
    }

}