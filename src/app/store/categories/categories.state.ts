import { CategoriesService } from '@services/categories.service';
import { Categories,CategoriesStateModel } from '@models/categories.model';
import { tap, catchError, throttleTime } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { GetAllCategories,SelectCategory } from './categories.action';
import { AlertProvider } from '@provider/alert-provider';
import { Navigate } from '@ngxs/router-plugin';


@State<CategoriesStateModel>({
    name: 'categories',
    defaults: {
      cat: null,
      cats: []
    }
  })

export class CategoriesState {

    @Selector()
    static getAllCategories(state: CategoriesStateModel) {
      return state.cats;
    }
  
    constructor(private store:Store,private categoriesService:CategoriesService,private alertProvider:AlertProvider) {
          
    }  

    @Action(GetAllCategories)
    getAllCategories({ patchState }: StateContext<CategoriesStateModel>, {  }: GetAllCategories) {
      return this.categoriesService.getAllCategories().pipe(
        tap((result: Categories[]) => {
          patchState({
            cats: result
          });
        }),
        catchError(err => {
          return throwError(err);
        })
      );
    }

    @Action(SelectCategory)
    selectCategory({ patchState }: StateContext<CategoriesStateModel>, { payload }: SelectCategory) {
      return this.categoriesService.selectCategories(payload).pipe(
        tap((result: Categories[]) => {
            this.alertProvider.selectedCategory()
        }),
        catchError(err => {
          return throwError(err);
        })
      );
    }
  



}