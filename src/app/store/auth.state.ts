import { AuthService } from '@services/auth.service';
import { UserService } from '@services/user.service';
import { Auth,AuthStateModel } from '@models/auth.model';
import { tap,catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { State,Action,StateContext,Selector,Store, StateStream } from '@ngxs/store';
import { Login,GetUserOwnProfile, ViewUserProfile,SignUp,EditProfile } from '@store/auth.action';
import { AlertProvider } from '@provider/alert-provider';
import { Navigate } from '@ngxs/router-plugin';
import { User,UserStateModel } from '@models/user.model';


@State<AuthStateModel>({
    name:'auth',
    defaults:{
        token:null,
        id:null,
        coverPhoto: null,
        provider: null,
        user:null,
        image:null
    }
})

@State<UserStateModel>({
    name:'user',
    defaults:{
        user:null,
        users:[]
    }
})


export class AuthState {
    @Selector()
    static token(state: AuthStateModel){
        return state.token
    }

    @Selector()
    static getUserId(state: AuthStateModel){
        return state.id
    }

    @Selector()
    static viewUserProfile(state:AuthStateModel){
        return state.user
    }

    @Selector()
    static getProfile(state:AuthStateModel){
        return state.image
    }

    @Selector()
    static getUserOwnProfile(state:UserStateModel){
        return state.user
    }

    constructor(private authService:AuthService,private store:Store,private alertProvider:AlertProvider,private userService:UserService)
    {

    }

    @Action(GetUserOwnProfile)
    getUserOwnProfile({patchState}:StateContext<UserStateModel>,{}: GetUserOwnProfile){
        return this.userService.getUserOwnProfile().pipe(
            tap((result:User)=> {
                patchState({
                    user: result
                })
                console.log(result)
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

   

    @Action(ViewUserProfile)
    viewUserProfile({patchState}:StateContext<UserStateModel>,{id}:ViewUserProfile){
        return this.userService.viewUserProfile(id).pipe(
            tap((result:User)=>{
                patchState({
                        user:result
                    })
                    console.log(result);
                }),
                catchError(err => {
                    return throwError(err);
                })
        )
    }


    @Action(SignUp)
    signUp({patchState}:StateContext<AuthStateModel>,{payload}:SignUp){
        return this.authService.register(payload).pipe(
            tap((result:Auth)=>{
                    patchState({
                         token:result.token.access_token,
                         id:result.profile.id,
                         image:result.profile.image  
                    });
                    this.store.dispatch(new Navigate(['/create-wallet']))
                    this.alertProvider.loadingController(result)
                    }), catchError(err => {
                        patchState({
                            token: null   
                        });
                        return throwError(err);
                    })
        )
    }

    @Action(EditProfile)
    editProfile({patchState}:StateContext<AuthStateModel>,{payload}:EditProfile){
        return this.authService.editProfile(payload).pipe(
            tap((result:Auth)=> {
                console.log(result);
                patchState({
                    user: result
                })
            }),
            catchError(err => {
                    return throwError(err);
            })
        )
    }


    @Action(Login)
    login({patchState}:StateContext<AuthStateModel>,{payload}:Login){
            return this.authService.login(payload).pipe(
                tap((result:Auth)=>{
                    patchState({
                        token:result.token.access_token,
                        id:result.profile.id,
                        image:result.profile.image
                    });
                    this.store.dispatch(new Navigate(['/tabs']))
                    this.alertProvider.login()
                }),
                catchError(err => {
                    patchState({
                      token: null
                    });
                    this.alertProvider.loginError()
                    return throwError(err);
                })
            )
    }

}