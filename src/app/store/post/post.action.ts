import { Post } from '../../models/post.model';
import { Comment } from '@models/comment.model';
import { Board } from '@models/board.model';

export class GetAllPost {
    static readonly type = '[Home] GetAllPosts'
}

export class GetAllBoards{
    static  readonly type = '[Pin Post] GetAllBoards'
}

export class GetTrendsToday {
    static readonly type = '[Search Page] GetAllTrendsToday'
    constructor(public data:string){}
}

export class GetPostImageById{
    static readonly type = '[ View User Profile Page ] GetPostPostImageById'
    constructor(public id:number){}
}

export class GetFavoritesToday{
     static readonly type = '[Search Page] GetAllFavoritesToday'
     constructor(public data:string){}
}

export class GetAllFavorites {
    static readonly type = '[Search Page] GetAllFavorites'
}

export class GetAllTrends {
    static readonly type = '[Search Page] GetAllTrends'
}

export class GetPostsPerPage {
    static readonly type = '[Home Page] GetPostsPerPage';
    constructor(public key: string, public pageNumber: string) {}
}

export class GetAllFeeds {
    static readonly type = '[Feeds] GetFeeds'
}

export class GetOwnPost {
    static readonly type ='[ User Profile ] GetOwnPost'
}

export class GetPostById {
    static readonly type = '[Image View] GetPostById';
    constructor (public id: number){}
}

export class CommentPost {
    static  readonly type = '[ Image View ] CommentPost';  
    constructor(public payload:Comment){}
}

export class LikePost {
    static readonly type = '[ Image View ] LikePost';
    constructor(public payload: Post) {}
}

export class DeletePost {
    static readonly type = '[ Delete Post ] Delete Post';
    constructor(public payload: Post){}
}

export class CreateBoard {
    static readonly type = '[ Create Board Modal ] CreateBoard';
    constructor(public payload: Board ){}
}

export class PinPost {
    static readonly type = '[ Pin Post Modal Page ] Pin Post';
    constructor (public payload: Board){}
}

export class CreatePost {
    static readonly type = '[ Post Image Page ]';
    constructor (public payload: FormData){}
}

export class SearchTrends {
    static readonly type = '[ Search Page ] Search';
    constructor (public payload: {val:string}){}
}




