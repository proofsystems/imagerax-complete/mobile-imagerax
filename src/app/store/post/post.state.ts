import { PostService } from '@services/post.service';
import { Post,PostStateModel } from '@models/post.model';
import { tap, catchError, throttleTime } from 'rxjs/operators';
import { throwError } from 'rxjs';
//NGXS
import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { Board,BoardStateModel } from '@models/board.model';
import { YourStateModel,Yours } from '@models/yours.model';
import { TrendStateModel,Trends } from '@models/trends.model';




import { 
    GetAllPost,
    GetPostById,
    CommentPost, 
    LikePost, 
    DeletePost, 
    GetOwnPost,
    GetAllFeeds,
    GetPostsPerPage,
    GetAllTrends,
    GetTrendsToday,
    GetAllFavorites,
    GetFavoritesToday,
    GetPostImageById,
    CreateBoard,
    GetAllBoards,
    PinPost,
    CreatePost,
    SearchTrends
} from './post.action';
import { CommentStateModel } from '@models/comment.model';
import { Feeds,FeedsStateModel } from '@models/feeds.model';
import { Navigate } from '@ngxs/router-plugin';
import { AlertProvider } from '@provider/alert-provider';


@State<PostStateModel>({
    /**
     * Always Check Model
     */
    name:'post',
    defaults:{
        posts:[],
        post:null,
        own_post:null,
        posts_feed:null,
        totalPages: null,
        itemsPerPage: null,
        currentPage: null,
        searchText: null
    }
})

@State<BoardStateModel>({
    name:'board',
    defaults:{
        board:null,
        boards:[],
        post:[]
    }
})

@State<TrendStateModel>({
    name:'trend',
    defaults:{
        trend:null,
        trends:[],
        searchText: null
    }
})

@State<YourStateModel>({
    name:'your',
    defaults:{
        your:null,
        yours:[]
    }

})

@State<FeedsStateModel>({
    name:'feeds',
    defaults:{
        feed:null,
        feeds:[]
    }
})

export class PostState {
   
        @Selector()
        static getAllPost(state:PostStateModel){
            return state.posts
        }
        
        @Selector()
        static filteredPosts(state:PostStateModel){
            return state.posts
            .filter((p: Post)=>{
                if(state.searchText){
                   if(p.caption.toLowerCase().includes(state.searchText.toLowerCase())){
                        return p;
                   }
                } else {
                    return p;
                }
            }) 
            .sort((a: any, b: any)=>{
                return a-b
            })
        }

        @Selector()
        static getOwnPosts(state:PostStateModel){
            return state.own_post
        }

        @Selector()
        static getAllFeeds(state:FeedsStateModel){
            return state.feeds
        }

        @Selector()
        static getTotalPages(state: PostStateModel) {
            return state.totalPages;
        }

        @Selector()
        static getItemsPerPage(state: PostStateModel) {
            return state.itemsPerPage;
        }

        @Selector()
        static getCurrentPage(state: PostStateModel) {
            return state.currentPage;
        }

        @Selector()
        static getAllTrends(state:PostStateModel){
            return state.posts
        }

        @Selector()
        static getAllFavorites(state:PostStateModel){
            return state.posts
        }

        @Selector()
        static getTrendsToday(state:TrendStateModel){
            return state.trends
        }

        @Selector()
        static getFavoritesToday(state:YourStateModel){
            return state.yours
        }

        @Selector()
        static getAllBoards(state:BoardStateModel){
            return state.boards
        }

        @Selector()
        static getPostById(state:PostStateModel){
            return state.post
        }

        @Selector()
        static getPostImageById(state:PostStateModel){
            return state.post
        }

        constructor(private postService: PostService,private store:Store,private alerProvider:AlertProvider){}

        @Action(GetPostImageById)
        getPostImageById({patchState}:StateContext<PostStateModel>,{id}:GetPostImageById){
            return this.postService.getPostImageById(id).pipe(
                tap((result:Post[])=>{
                    {
                        patchState({
                            posts:result
                        })
                    }
                }), catchError(err => {
                        return throwError(err)
                })
            )
        }

        @Action(SearchTrends)
        setFilterTrends({ patchState }: StateContext<PostStateModel>, { payload }: SearchTrends) {
            patchState({
              searchText: payload.val
            });
        }

        @Action(GetFavoritesToday)
        getFavoritesToday({patchState}: StateContext<YourStateModel>,{data}:GetFavoritesToday){
            return this.postService.getAllFavoritesToday(data).pipe(
                tap((result:Yours[])=>{
                    {
                        patchState({
                                yours:result
                        })
                        if(result.length== 0){
                        } else {
    
                        }
                    }
                }), catchError(err => {
                        return throwError (err)
                })
            )

        }

        @Action(GetTrendsToday)
        getTrendsToday({patchState}:StateContext<TrendStateModel>,{data}: GetTrendsToday){
            return this.postService.getAllTrendsToday(data).pipe(
                tap((result:Trends[])=>
                    {
                    patchState({
                        trends:result
                        
                    })
                    if(result.length== 0){
                    } else {

                    }
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(GetPostById)
        getPostById(ctx: StateContext<PostStateModel>, { id }: GetPostById) {
          return this.postService.getPostById(id).pipe(
            tap((result: Post) => {
              console.log(result);
              ctx.patchState({
                post: result
              });
            }),
            catchError(err => {
              console.log(err);
              return throwError(err);
            })
          );
        }

        @Action(GetAllBoards)
        getAllBoards({patchState}:StateContext<BoardStateModel>,{} : GetAllBoards){
            return this.postService.getAllBoard().pipe(
                tap((result:Board[])=>{
                    patchState({
                        boards:result
                    })
                    console.log(result)
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(GetAllTrends)
        getAllTrends({patchState}:StateContext<PostStateModel>,{}: GetAllTrends){
            return this.postService.getAllTrends().pipe(
                tap((result:Post[])=>{
                    patchState({
                        posts:result
                    })
                    console.log(result)
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(GetAllFavorites)
        getAllFavorites({patchState}:StateContext<PostStateModel>,{}: GetAllFavorites){
            return this.postService.getAllFavorites().pipe(
                tap((result:Post[])=>{
                    patchState({
                        posts:result
                    })
                    console.log(result)     
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(GetAllPost)
        getAllPosts({patchState}: StateContext<PostStateModel>,{ }: GetAllPost){
            return this.postService.getAll().pipe(
                tap((result:Post[])=>{ 
                    patchState({
                        posts:result
                    })
                    console.log(result);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(GetPostsPerPage)
        getPostsPerPage(ctx: StateContext<PostStateModel>, { key, pageNumber }: GetPostsPerPage) {
          return this.postService.getPostPerPage(key, pageNumber).pipe(
            tap((result: Post[]) => {
              console.log(result);
              ctx.patchState({
                posts: result['data'],
                totalPages: result['total'],
                itemsPerPage: result['per_page'],
                currentPage: result['current_page']
              });
            }),
            catchError(err => {
              console.log(err);
              return throwError(err);
            })
          );
        }

        @Action(GetAllFeeds)
        getAllFeeds({patchState}: StateContext<FeedsStateModel>,{ }: GetAllFeeds){
            return this.postService.getFeeds().pipe(
                tap((result:Feeds[])=>{ 
                    patchState({
                        feeds:result
                    })
                    console.log(result);
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }


        @Action(GetOwnPost)
        getOwnPosts({patchState}:StateContext<PostStateModel>,{ }: GetOwnPost){
            return this.postService.getOwnPost().pipe(
                tap((result:Post[])=>{
                    patchState({
                        own_post:result
                    })
                    console.log(result)
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }       
        

      

        @Action(CommentPost)
        commmentPost({patchState}:StateContext<CommentStateModel>,{payload}:CommentPost){
            return this.postService.commentPost(payload).pipe(
                tap(()=> {}),
                catchError(err => {
                    console.log(err)
                    return throwError(err)
                })
            )
        }

        @Action(CreatePost)
        createPost({patchState}:StateContext<PostStateModel>,{payload}:CreatePost){
            return this.postService.createPost(payload).pipe(
                tap((result:any)=>{
                    this.alerProvider.postCreated(result['message'])
                    this.store.dispatch(new Navigate(['/tabs']))
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }

        @Action(CreateBoard)
        createBoard({patchState}:StateContext<BoardStateModel>,{payload}:CreateBoard){
            return this.postService.createBoard(payload).pipe(
                tap((result:any)=>{
                    this.alerProvider.boardCreated(result['message'])
                    this.store.dispatch(new Navigate(['/tabs']))
                }),
                catchError(err => {
                    return throwError(err)
                })
            )
        }



        
        @Action(PinPost)
        pinPost({patchState}:StateContext<BoardStateModel>,{payload}: PinPost){
            return this.postService.pinPost(payload).pipe(
                tap((result:Board)=>{
                    this.alerProvider.boardCreated(result['message'])
                    this.store.dispatch(new Navigate(['user-profile']))
                }),catchError(err => {
                    return throwError (err)
                })
            )
        }

        @Action(LikePost)
        likePost({patchState}:StateContext<PostStateModel>,{payload}:LikePost){
            return this.postService.likePost(payload).pipe(
                tap(()=> {}),
                catchError(err => {
                    console.log(err)
                    return throwError(err)
                })
            )
        }

        @Action(DeletePost)
        deletePost( {patchState}:StateContext<PostStateModel>,{payload}:DeletePost ){
            return this.postService.deletePost(payload).pipe(
                tap(() => {
                  console.log(payload);
                  this.store.dispatch(new Navigate(['/tabs']))
                }),
                catchError(err => {
                    console.log(err);
                    return throwError
                })
            )
        }        
}
