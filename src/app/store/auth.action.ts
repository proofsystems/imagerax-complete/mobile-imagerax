import { Auth } from '@models/auth.model';

export class Login {
    static readonly type = '[Login Page] LoginUser';
    constructor(public payload:Auth) {}
}

export class SignUp {
    static readonly type = '[Sign Up Page] SignUpUser';
    constructor(public payload: Auth) {}
}

export class ViewUserProfile {
    static readonly type = '[ View User Profile Page ] ViewUserProfile';
    constructor(public id:number){}
}

export class EditProfile {
    static readonly type  = '[ Edit Profile Page ] EditProfilePage';
    constructor(public payload: FormData){}
}

export class GetUserOwnProfile {
    static readonly type = '[ User Profile Page ] GetUserOwnProfile';
}

