import { NotificationService } from '@services/notification.service';
import { tap,catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { State,Action,StateContext,Selector,Store, StateStream } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { Notification,NotificationStateModel } from '@models/notification.model';
import { GetAllNotifications,GetRecentNotifications } from '@store/notifications/notifications.action';


@State<NotificationStateModel>({
    name:'notificaton',
    defaults:{
        notifications:[],
        recentNotifications:[]
    }
})

export class NotificationState{
        

    @Selector()
    static getAllNotification(state:NotificationStateModel){
        return state.notifications
    }

    @Selector()
    static getRecent10Notification(state:NotificationStateModel){
        return state.recentNotifications
    }

    constructor(
        private store: Store,
        private notificationService:NotificationService
    ){
      
    }       

    @Action(GetAllNotifications)
    getAllNotification({patchState}:StateContext<NotificationStateModel>,{}: GetAllNotifications){
        return this.notificationService.getAllNotications().pipe(
            tap((result:Notification[])=>{
                patchState({
                    notifications:result
                })
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }


}