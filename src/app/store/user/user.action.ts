import { Auth } from '@models/auth.model';

export class GetFollowers {
    static readonly type = '[ User Followers Modal ] Followers User Modal';
    constructor(public id:number){}
}

export class GetFollowing {
    static readonly type = '[ User Following Modal ] Following User Modal';
    constructor(public id:number){}
}

export class FollowUser {
    static readonly type = '[ View Profile Page ] FollowUser'
    constructor(public id:number){}
}

export class GetUserBoardOwned {
    static readonly type = '[User Profile Page] GetUserBoardOwned'
}

export class GetUserBoardById {
    static readonly type = '[Board View Page] GetUserBoardById'
    constructor(public id:number){}
}

export class UpdateProfile {
    static readonly type = '[ Edit Profile Page ]';
    constructor( public payload:FormData ){}
}

export class UpdateCoverPhoto {
    static readonly type = '[ Edit Profile Page ]';
    constructor( public payload:FormData ){}
}  