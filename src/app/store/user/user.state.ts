import { UserService } from '@services/user.service';
import { tap,catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { GetUserOwnProfile } from '@store/auth.action';
import { State,Action,StateContext,Selector,Store, StateStream } from '@ngxs/store';
import { GetFollowers,GetFollowing,FollowUser,GetUserBoardOwned,GetUserBoardById,UpdateProfile,UpdateCoverPhoto } from '@store/user/user.action';
import { AlertProvider } from '@provider/alert-provider';
import { Navigate } from '@ngxs/router-plugin';
import { User,UserStateModel } from '@models/user.model';
import { resource } from 'selenium-webdriver/http';
import { Board,BoardStateModel } from '@models/board.model';

@State<UserStateModel>({
        name:'user',
        defaults:{
            user:null,
            users:[],
        }
})

@State<BoardStateModel>({
    name:'board',
    defaults:{
        board:null,
        boards:[],
        post:[]
    }
})

export class UserState {

    @Selector()
    static getUserBoardOwned(state:BoardStateModel){
        return state.boards
    }

    @Selector()
    static getUserBoardById(state:BoardStateModel){
        return state.board
    }

    @Selector()
    static getAllFollowers(state:UserStateModel){
        return state.users
    }

    @Selector()
    static getAllFollowing(state:UserStateModel){
        return state.users
    }

    constructor(
        private userService:UserService,
        private store:Store,
        private alertProvider:AlertProvider
    )
    {}

    @Action(GetUserBoardById)
    getUserBoardOwnedById(ctx:StateContext<BoardStateModel>,{id}: GetUserBoardById){
        return this.userService.getUserBoardById(id).pipe(
            tap((result:Board)=>{
                console.log(result)
                ctx.patchState({
                    board:result
                });
            }), catchError(err => {
                console.log(err)
                return throwError(err)
            })
        )
    }

    @Action(UpdateProfile)
    updateProfile({patchState}:StateContext<User>,{payload}:UpdateProfile){
        return this.userService.updateProfile(payload).pipe(
            tap(()=>{
                this.store.dispatch(new Navigate(['/user-profile']))
                this.alertProvider.updateProfile();
            }), catchError(err => {
                  return throwError(err)  
            })
        )
    }

    @Action(UpdateCoverPhoto)
    updateCoverPhoto({patchState}:StateContext<User>,{payload}:UpdateProfile){
        return this.userService.updateProfile(payload).pipe(
            tap(()=>{
            }), catchError(err => {
                  return throwError(err)  
            })
        )
    }
    
    @Action(GetUserBoardOwned)
    getUserBoardOwned({patchState}:StateContext<BoardStateModel>): GetUserBoardOwned{
        return this.userService.getUserBooardOwned().pipe(
            tap((result:Board[])=>{
                    patchState({
                        boards:result,
                    })
            }), catchError(err => { 
                    return throwError(err)
            })
        )
    }

    @Action(GetFollowers)
    getAllFollowers({patchState}: StateContext<UserStateModel>,{ id }: GetFollowers){     
        return this.userService.getFollowers(id).pipe(
            tap((result:User[])=>{
                patchState({
                    users:result
                })
            }), catchError(err => {
                return throwError(err)
            })
        )
    }

    

    @Action(GetFollowing)
    getAllFollowing({patchState}: StateContext<UserStateModel>,{id} : GetFollowing){
        return this.userService.getFollowing(id).pipe(
            tap((result:User[])=>{
                patchState({
                    users:result
                })
            }), catchError(err => {
                    return throwError(err)
            })
        )
    }

    @Action(FollowUser)
    followUSer({patchState}:StateContext<UserStateModel>,{id}:FollowUser){
        return this.userService.onFollowUser(id).pipe(
            tap((result:any)=>{
                this.alertProvider.boardCreated(result['message'])
            }), catchError(err => {
                return throwError(err)
            })
        )
    }

}

