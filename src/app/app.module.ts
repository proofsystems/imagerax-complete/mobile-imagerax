import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from '../app/core/core.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@environments/environment';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { UserFollowPageModule } from '@pages/user/user-follow/user-follow.module';
import { UserFollowingPageModule } from '@pages/user/user-following/user-following.module';
import { ImageModalPageModule } from '@pages/modal/image-modal/image-modal.module';
import { CreateBoardPageModule } from '@pages/modal/create-board/create-board.module';
import { PinPostModalPageModule } from '@pages/modal/pin-post-modal/pin-post-modal.module';
import { SelectCategoryPageModule } from '@pages/modal/select-category/select-category.module'
import { FileTransfer, FileTransferObject,FileUploadOptions } from '@ionic-native/file-transfer/ngx'
import { Camera } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
  AppRoutingModule,
  ServiceWorkerModule.register('ngsw-worker.js',{enabled: environment.production}),
  CoreModule,
  HttpClientModule,
  LazyLoadImageModule,
  FontAwesomeModule,
  UserFollowPageModule,
  ImageModalPageModule,
  UserFollowingPageModule,
  CreateBoardPageModule,
  PinPostModalPageModule,
  SelectCategoryPageModule,
  IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FileTransfer,
    FileTransferObject,
    Crop,
    Camera,
    WebView,
    FilePath,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
