import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/core/guard/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'login', 
    loadChildren: './pages/login/login.module#LoginPageModule' ,
    
  },
  { 
      path: 'post-view/:id', 
      loadChildren: () => import('./pages/post/post-view/post-view.module').then(m => m.PostViewPageModule),
      canActivate: [AuthGuard] 
  },
  { 
    path: 'image-modal', 
    loadChildren: './pages/modal/image-modal/image-modal.module#ImageModalPageModule' },
  { 
    path: 'search', 
    loadChildren: () => import('@pages/search/search.module').then(m => m.SearchPageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'user-profile', 
    loadChildren: () => import('./pages/user/user-profile/user-profile.module').then(m => m.UserProfilePageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'user-follow', 
    loadChildren: () => import('@pages/user/user-follow/user-follow.module').then(m => m.UserFollowPageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'view-user-profile/:id', 
    loadChildren:() => import('@pages/user/view-user-profile/view-user-profile.module').then(m => m.ViewUserProfilePageModule),
    canActivate: [AuthGuard] 

  },
  { 
    path: 'user-following', 
    loadChildren: './pages/user/user-following/user-following.module#UserFollowingPageModule' },
  { 
    path: 'user-update-profile', 
    loadChildren: './pages/user/user-update-profile/user-update-profile.module#UserUpdateProfilePageModule' 
  },
  { 
    path: 'register', 
    loadChildren: () => import('@pages/register/register.module').then(m => m.RegisterPageModule)
  },
  { 
    path: 'notification', 
    loadChildren: './pages/notification/notification.module#NotificationPageModule' 
  },
  { 
    path: 'create-board', 
    loadChildren: './pages/modal/create-board/create-board.module#CreateBoardPageModule' }
    ,
  { 
    path: 'pin-post-modal', 
    loadChildren: './pages/modal/pin-post-modal/pin-post-modal.module#PinPostModalPageModule' 
  },
  // { path: 
  //   'post-image', 
  //   loadChildren: () => import('@pages/post/post-image/post-image.module').then(m => m.PostImagePageModule)
  // },
  { 
    path: 'board-view/:id', 
    loadChildren:() => import('@pages/board/board-view/board-view.module').then(m => m.BoardViewPageModule)
  },
  { path: 'post-video', 
    loadChildren:() => import('@pages/post/post-video/post-video.module').then(m => m.PostVideoPageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'create-wallet', 
    loadChildren:() => import('./pages/create-wallet/create-wallet.module').then(m => m.CreateWalletPageModule),
    canActivate: [AuthGuard] 
  },
  { 
    path: 'sirius-login', 
    loadChildren:() => import('./pages/sirius/sirius-login/sirius-login.module').then(m => m.SiriusLoginPageModule),
  },
  { 
    path: 'post-image', 
    loadChildren:() => import('./pages/post/post-image/post-image.module').then(m => m.PostImagePageModule),
    canActivate: [AuthGuard] 
  },  { path: 'select-category', loadChildren: './pages/modal/select-category/select-category.module#SelectCategoryPageModule' },


  
  
  

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
